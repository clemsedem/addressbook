defmodule AddressBook.Repo.Migrations.CreateCountries do
  use Ecto.Migration

  def up do
      create table(:countries) do
        add :country_name, :string
        add :country_code, :string, size: 3
        add :active_status, :boolean, default: true
        add :del_status, :boolean, default: false
        add :created_at, :utc_datetime
        add :updated_at, :utc_datetime
      end
  end

  def down do
    drop table(:countries)
  end

end
