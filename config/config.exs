use Mix.Config

config :address_book, AddressBook.Repo,
   database: "address_book",
   username: "clem",
   password: "$Cl3m@200!",
   hostname: "localhost"

config :address_book, ecto_repos: [AddressBook.Repo]