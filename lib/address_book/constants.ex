defmodule AddressBook.Constants do
  @err_missing_country_name %{resp_code: "111", resp_desc: "Missing country name"}
  @err_missing_country_code %{resp_code: "112", resp_desc: "Missing country code"}
  @err_missing_country_id %{resp_code: "115", resp_desc: "Missing country id"}
  @err_country_code_exist %{resp_code: "113", resp_desc: "Country code already exist"}
  @err_country_created_successfully %{resp_code: "000", resp_desc: "Country created successfully"}
  @err_country_updated_successfully %{resp_code: "000", resp_desc: "Country updated successfully"}
  @err_record_not_found %{resp_code: "999", resp_desc: "Record not found!"}




  ############################################################################################
  ############################################################################################
  def err_missing_country_name, do: @err_missing_country_name
  def err_missing_country_code, do: @err_missing_country_code
  def err_country_code_exist, do: @err_country_code_exist
  def err_country_created_successfully, do: @err_country_created_successfully
  def err_country_updated_successfully, do: @err_country_updated_successfully
  def err_record_not_found, do: @err_record_not_found
  def err_missing_country_id, do: @err_missing_country_id

end