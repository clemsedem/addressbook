defmodule AddressBook.Schema.Country do
    use Ecto.Schema
    import Ecto.Changeset

    schema "countries" do
      field :country_name, :string
      field :country_code, :string
      field :active_status, :boolean
      field :del_status, :boolean
      field :created_at, :utc_datetime
      field :updated_at, :utc_datetime
    end

    def changeset(countryreq, params \\ %{}) do
      countryreq
      |> cast(params, [:country_name, :country_code, :active_status, :del_status, :created_at, :updated_at])
      |> unique_constraint(:country_code)
    end

end