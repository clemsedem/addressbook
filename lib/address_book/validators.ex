defmodule AddressBook.Validators do
    alias AddressBook.Constants
#    alias AddressBook.EctoRoutines

    def valid_country_name(nil), do: {:error, Constants.err_missing_country_name()}
    def valid_country_name(""), do: {:error, Constants.err_missing_country_name()}
    def valid_country_name(country_name) do
      if is_binary(country_name) do
        {:ok, country_name}
      else
        {:error, Constants.err_missing_country_name() }
      end
    end

    def valid_country_code(nil), do: {:error, Constants.err_missing_country_code()}
    def valid_country_code(""), do: {:error, Constants.err_missing_country_code()}
    def valid_country_code(country_code) do
      if is_binary(country_code)  do
        {:ok, country_code}
      else
        {:error, Constants.err_missing_country_code() }
      end
    end

    def valid_country_id(nil), do: {:error, Constants.err_missing_country_id()}
    def valid_country_id(""), do: {:error, Constants.err_missing_country_id()}
    def valid_country_id(country_id) do
      if is_integer(country_id)  do
        {:ok, country_id}
      else
        {:error, Constants.err_missing_country_id() }
      end
    end


end