defmodule AddressBook.Router  do
  alias AddressBook.EctoRoutines
  alias AddressBook.Validators
  alias AddressBook.Constants

  use Plug.Router
  use Plug.Debugger
  require Logger

  plug(Plug.Logger, log: :debug)
  plug(:match)
  plug(:dispatch)



#    Individual routes go here

##############################################################################
# ROUTES FOR COUNTRIES
###############################################################################
#    list contacts
  post "/countries" do
    res = EctoRoutines.listCountries()
    IO.puts "\nThese are the contacts \n"
    IO.inspect res
#    {:ok, res_json} = Poison.encode(res)
#    IO.inspect res_json
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(res))
  end

   post "/new_country" do
    {:ok, body, _conn} = read_body(conn)

    {:ok, parsed_body} = Poison.decode(body)
    IO.inspect parsed_body["country_name"]
    with {:ok, country_name} <- Validators.valid_country_name(parsed_body["country_name"]),
         {:ok, country_code} <- Validators.valid_country_code(parsed_body["country_code"])
    do
      case EctoRoutines.createCountry(country_name, country_code) do
        {:ok, struct} -> #inserted with success
          IO.puts "\nSuccessfully saved\n"
          IO.inspect struct
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Poison.encode!(Constants.err_country_created_successfully()))
        {:error, changeset} ->
          IO.puts "\nFailed to save\n"
          IO.inspect changeset
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Poison.encode!(Constants.err_country_code_exist()))
      end
    else
      {:error, reason} ->
        conn
        |> send_resp(200, Poison.encode!(reason))
        |> halt
    end
  end


  post "/edit_country" do
    Logger.info "Starting edit country route..."
    {:ok, body, _conn} = read_body(conn)

    {:ok, parsed_body} = Poison.decode(body)
    IO.inspect parsed_body["country_code"]
    with {:ok, country_code} <- Validators.valid_country_code(parsed_body["country_code"])
    do
       res = EctoRoutines.editCountry(country_code)
       IO.inspect res
       if length(res) ==1 do
         conn
         |> put_resp_content_type("application/json")
         |> send_resp(200, Poison.encode!(res))
       else
         conn
         |> put_resp_content_type("application/json")
         |> send_resp(200, Poison.encode!(Constants.err_record_not_found()))
       end

    else
      {:error, reason} ->
        conn
        |> send_resp(200, Poison.encode!(reason))
        |> halt
    end
    conn
    |> send_resp(200, "Ok")
  end

#  Update country
  post "update_country" do
    Logger.info "Starting edit country route..."
    {:ok, body, _conn} = read_body(conn)

    {:ok, parsed_body} = Poison.decode(body)
    IO.inspect parsed_body["country_code"]

    with {:ok, country_name} <- Validators.valid_country_name(parsed_body["country_name"]),
         {:ok, country_code} <- Validators.valid_country_code(parsed_body["country_code"]),
         {:ok, id} <- Validators.valid_country_id(parsed_body["id"])
      do
      case EctoRoutines.updateCountry(id, country_name, country_code) do
        {:ok, struct} -> #inserted with success
          IO.puts "\nSuccessfully updated\n"
          IO.inspect struct
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Poison.encode!(Constants.err_country_updated_successfully()))
        {:error, changeset} ->
          IO.puts "\nFailed to update\n"
          IO.inspect changeset
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Poison.encode!(changeset)) #%{resp_code: "989", resp_desc: "Record does not exist!"}
      end
    else
      {:error, reason} ->
        conn
        |> send_resp(200, Poison.encode!(reason))
        |> halt
    end
  end

  post "/delete_country" do
    Logger.info "Starting to delete a country..."
    {:ok, body, _conn} = read_body(conn)
    {:ok, parsed_body} = Poison.decode(body)

    with {:ok, id} <- Validators.valid_country_id(parsed_body["id"]) do
      case EctoRoutines.deleteCountry(id) do
        {:ok, struct} ->
          IO.puts "\nSuccessfully updated\n"
          IO.inspect struct
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Poison.encode!(Constants.err_country_updated_successfully()))
        {:error, changeset} ->
          IO.puts "\nFailed to update\n"
          IO.inspect changeset
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Poison.encode!(changeset))
      end
    else
      {:error, reason} ->
      conn
      |> send_resp(200, Poison.encode!(reason))
      |> halt
    end

  end
  ##############################################################################
  # ROUTES FOR COUNTRIES
  ###############################################################################

  ##############################################################################
  # ROUTES FOR CITIES
  ###############################################################################


  ##############################################################################
  # ROUTES FOR CITIES
  ###############################################################################


#  404 route
    match _ do
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(404, Poison.encode!(%{
         resp_code: "404",
         resp_desc: "Request not found!"
      }))
    end
end