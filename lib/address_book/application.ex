defmodule AddressBook.Application do
  @moduledoc false

  use Application
  require Logger

  def start(_type, _args) do
    children = [
      AddressBook.Repo,
      Plug.Adapters.Cowboy.child_spec(scheme: :http, plug: AddressBook.Router, options: [port: 7000])
    ]

    opts = [strategy: :one_for_one, name: AddressBook.Supervisor]

    Logger.info("Starting application...")

    Supervisor.start_link(children, opts)
  end
end
