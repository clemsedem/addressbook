defmodule AddressBook.EctoRoutines do
  alias AddressBook.Repo
  alias AddressBook.Schema.Country
#  alias AddressBook.Constants

  import Ecto.Query

#  Listing all active countries
  def listCountries do
    query = from(c in Country,  where: c.active_status == true and c.del_status == false, order_by: [desc: c.created_at])
     |> Repo.all
     if length(query) > 0 do
        query
    else
       IO.puts msg="\nNo records founds"
       {:error, msg}
    end
  end

#  create new country
  def createCountry(country_name, country_code) do
      country_code = String.upcase(country_code)
      query = from(c in Country, where: c.country_code == ^country_code and c.active_status == true)
      if !Repo.exists?(query) do
        res = %Country{country_name: country_name, country_code: country_code}
        |> Repo.insert
        res
      else
        IO.puts msg="\n Code already exists"
        {:error, msg}
      end
   end

#   get country to edit
   def editCountry(country_code) do
    country_code = String.upcase(country_code)
    query = from(c in Country, where: c.country_code == ^country_code and c.active_status == true)
    |> Repo.all
    query
  end

#  Country update (patch)
  def updateCountry(id, country_name, country_code) do
    country_code = String.upcase(country_code)
    record = Repo.get(Country, id)
    IO.puts "\n##################################"
    IO.inspect record
    IO.puts "\n##################################"
    if !is_nil(record) do
      code_exist = from(c in Country, where: c.country_code == ^country_code and c.active_status == true)
      if !Repo.exists?(code_exist) do
        changeset = Country.changeset(record, %{country_name: country_name, country_code: country_code, updated_at: NaiveDateTime.utc_now })
        res = Repo.update(changeset)
        res
      else
        IO.puts "\n"
        IO.puts msg="Code already exist"
        {:error, msg}
      end
    else
      IO.puts "\n"
      IO.puts msg="Record does not exist"
      {:error, msg}
    end
  end

  def deleteCountry(id) do
    record = Repo.get(Country, id)
    if !is_nil(record) do
      changeset = Country.changeset(record, %{active_status: false, del_status: true, updated_at: NaiveDateTime.utc_now })
      res = Repo.update(changeset)
      res
    else
      IO.puts "\n"
      IO.puts msg="Record does not exist"
      {:error, msg}
    end
  end


end